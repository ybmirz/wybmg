"use client";
import React, { useState } from 'react'
export default function Home() {
  const [answer, setAnswer] = useState<string>("");
  
  const handleYesClick = () => {
    setAnswer("Let's fucking goooo!!!!");
  }

  const handleNoClick = () => {
    window.location.reload()
  }

  return (
    <div className="text-center flex h-screen">
      <div className="m-auto">
        <h1 className="text-3xl font-bold mb-8">Will you be my girlfriend?</h1>
        <div className="flex justify-center items-center">
          <button
            className="px-4 py-2 bg-green-500 text-white rounded mr-4"
            onClick={handleYesClick}
          >
            Yes
          </button>
          <button
            className="px-4 py-2 bg-red-500 text-white rounded"
            onClick={handleNoClick}
          >
            No
          </button>
        </div>
        {answer && <p className="text-xl mt-8 text-white">{answer}</p>}
      </div>
    </div>
  );
}
